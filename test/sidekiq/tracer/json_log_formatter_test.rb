require "test_helper"

class Sidekiq::Tracer::JsonLogFormatterTest < Minitest::Test

  class LoggingWorker
    include Sidekiq::Worker
    def perform
      logger.error("Holy smokes Batman!")
    end
  end

  def test_json_log_format
    file = StringIO.new
    with_static_values { LoggingWorker.perform_async }
    Sidekiq::Logging.with_context({foo: "bar"}) do
      with_logger(file) do
        with_static_values { LoggingWorker.drain }
      end
    end
    assert_equal File.read("test/fixtures/example.json"), file.string
  end

  private

  def with_static_values
    Timecop.freeze(DateTime.new(1990, 1, 23, 5, 24)) do
      Sidekiq.logger.formatter.stub(:pid, 76543) do
        Sidekiq.logger.formatter.stub(:thread_id, "2n9c") do
          Thread.current[:sidekiq_root_trace] = ["123abc-456def-789ghi"]
          yield
          Thread.current[:sidekiq_root_trace] = []
        end
      end
    end
  end

  def with_logger(file)
    previous_logger = Sidekiq.logger
    Sidekiq.logger = Logger.new(file)
    Sidekiq.logger.formatter =
      Sidekiq::Tracer::JsonLogFormatter.new(pretty: true)
    yield
    Sidekiq.logger = previous_logger
  end
end

require "test_helper"

class Sidekiq::TracerTest < Minitest::Test
  def setup
    Sidekiq::Worker.clear_all
  end

  def test_middleware_creates_tracing_id
    stubbed_uuid("foobar") { DummyWorker.perform_async }
    stubbed_uuid("blegga") { DummyWorker.perform_async }
    assert_traces %w(foobar blegga), DummyWorker
  end

  def test_middleware_uses_existing_trace
    stubbed_uuid("foobar") { CascadingWorker.perform_async(2) }
    stubbed_uuid("blegga") { CascadingWorker.perform_async(1) }
    CascadingWorker.drain
    assert_traces %w(foobar foobar blegga), DummyWorker
  end

  private

  def assert_traces(expected, worker_class)
    traces = worker_class.jobs.map { |j| j["root_trace"] }
    assert_equal expected, traces
  end

  def stubbed_uuid(value)
    SecureRandom.stub(:uuid, value) { yield }
  end
end

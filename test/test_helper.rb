$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "pry"
require "timecop"
require "sidekiq/testing"
require "sidekiq/tracer/auto"
require "minitest/autorun"

class DummyWorker
  include Sidekiq::Worker

  def perform(*args)
    args
  end
end

class CascadingWorker
  include Sidekiq::Worker

  def perform(n)
    n.times { DummyWorker.perform_async }
  end
end

Sidekiq::Testing.server_middleware do |chain|
  chain.add Sidekiq::Tracer::ServerMiddleware
end

Timecop.safe_mode = true

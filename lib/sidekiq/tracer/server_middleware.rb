module Sidekiq
  module Tracer
    class ServerMiddleware
      def call(worker, msg, queue)
        with_trace(msg["root_trace"]) { yield }
      end

      private

      def with_trace(value)
        Thread.current[:sidekiq_root_trace] ||= []
        Thread.current[:sidekiq_root_trace] << value unless value.nil?
        yield
      ensure
        Thread.current[:sidekiq_root_trace].pop
      end
    end
  end
end

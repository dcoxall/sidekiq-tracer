module Sidekiq
  module Tracer
    class ClientMiddleware
      def call(_worker_class, job, _queue, _redis_pool)
        job["root_trace"] = root_trace
        yield
      end

      private

      def root_trace
        if Thread.current[:sidekiq_root_trace] &&
            Thread.current[:sidekiq_root_trace].last
          return Thread.current[:sidekiq_root_trace].last
        end
        SecureRandom.uuid
      end
    end
  end
end

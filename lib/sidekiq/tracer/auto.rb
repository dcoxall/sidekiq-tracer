require "sidekiq/tracer"

Sidekiq.logger.formatter = Sidekiq::Tracer::JsonLogFormatter.new

Sidekiq.configure_client do |config|
  config.client_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ClientMiddleware
  end
end

Sidekiq.configure_server do |config|
  config.client_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ClientMiddleware
  end
  config.server_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ServerMiddleware
  end
end

require "multi_json"

module Sidekiq
  module Tracer
    class JsonLogFormatter < ::Logger::Formatter
      def initialize(pretty: false)
        @pretty = !!pretty
      end

      def call(severity, time, _progname, msg)
        MultiJson.dump({
          tms: time.utc.iso8601(3),
          pid: pid,
          tid: format("TID-%s", thread_id),
          cxt: context,
          sev: severity,
          msg: msg,
          trc: trace,
        }, pretty: @pretty) + "\n"
      end

      private

      def pid
        ::Process.pid
      end

      def thread_id
        Thread.current.object_id.to_s(36)
      end

      def context
        Thread.current[:sidekiq_context]
      end

      def trace
        trace_id = Thread.current[:sidekiq_root_trace]
        trace_id.last unless trace_id.nil?
      end
    end
  end
end

[![build status](https://gitlab.com/dcoxall/sidekiq-tracer/badges/master/build.svg)](https://gitlab.com/dcoxall/sidekiq-tracer/commits/master)

# Sidekiq Tracer

Sidekiq Tracer aims to provide better logging for sidekiq jobs to make debugging issues easier. It does this by either generating a tracing id which is included in the logs. If a job is then created during the execution of another then the tracing id is passed down.

On it's own it does little but when the tracing ID is pulled into logs then it becomes possible to follow complex chains of jobs when 1 worker will queue up multiple others.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sidekiq-tracer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sidekiq-tracer

## Usage

The easiest way to configure and use sidekiq-tracer is to add the following to you Gemfile

```ruby
gem "sidekiq-tracer", require: "sidekiq/tracer/auto"
```

This will then require and configure the gem in the standard way.

If you want more control over the configuration however then include sidekiq-tracer in your Gemfile
in the standard way and then use the following to enable it for Sidekiq.

```ruby
require "sidekiq/tracer"

Sidekiq.logger.formatter = Sidekiq::Tracer::JsonLogFormatter.new

Sidekiq.configure_client do |config|
  config.client_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ClientMiddleware
  end
end

Sidekiq.configure_server do |config|
  config.client_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ClientMiddleware
  end
  config.server_middleware do |chain|
    chain.prepend Sidekiq::Tracer::ServerMiddleware
  end
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

First, please read and understand the Code of Conduct for this project. This project is open to contribution from all and to ensure that everyone feels welcome then following the code of conduct is very important.

Bug reports and pull requests are welcome at https://gitlab.com/dcoxall/sidekiq-tracer.

When reporting an issue be descriptive and provide as much information as possible to re-create it.

When submitting a pull request then feel free to create the request early and ask for feedback or advice. New additions should be covered with tests and the community should be more than happy to help if you are unsure how best to achieve that.

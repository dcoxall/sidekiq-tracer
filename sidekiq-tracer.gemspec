# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sidekiq/tracer/version'

Gem::Specification.new do |spec|
  spec.name          = "sidekiq-tracer"
  spec.version       = Sidekiq::Tracer::VERSION
  spec.authors       = ["Darren Coxall"]
  spec.email         = ["darren@darrencoxall.com"]
  spec.licenses      = ["MIT"]

  spec.summary       = %q{Sidekiq middleware to better trace how jobs are executed}
  spec.description   = %q{Sidekiq middleware that adds JSON logging and job tracing for better debugging}
  spec.homepage      = "https://gitlab.com/dcoxall/sidekiq-tracer"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version = ">= 2.0"

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "pry", "> 0"
  spec.add_development_dependency "timecop", "~> 0.8.0"
  spec.add_runtime_dependency     "sidekiq", ">= 2.0"
  spec.add_runtime_dependency     "multi_json", "~> 1.0"
end
